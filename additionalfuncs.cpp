#include "additionalfuncs.h"

bool ascending(int a, int b) { return a > b; }

bool descending(int a, int b) { return a < b; }

void generateValues(std::vector<int>& vectorValues, int* arrayValues,
                    std::size_t length) {
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::random_device rd;
    std::mt19937 mersenne(rd());

    std::cout << "Enter the starting value of the range of numbers - ";
    int start = getIntValue();

    std::cout << "Enter the ending value of the range of numbers - ";
    int end = getIntValue();

    // Указать диапазон целых чисел, по умолчанию int
    std::uniform_int_distribution<> dist(start, end);
    vectorValues.resize(length);

    for (std::size_t i{}; i < length; ++i) {
        int tempValue = dist(mersenne);
        arrayValues[i] = tempValue;
        vectorValues.at(i) = tempValue;
        std::cout << "Value " << (i + 1) << ") - " << vectorValues[i] << '\n';
    }
}

std::size_t enterLength() {
    std::cout << "How many values would you like to enter?"
              << "\n";

    return static_cast<std::size_t>(getIntValue());
}

int getIntValue() {
    int value = 0;
    while (true) {
        std::cin >> value;

        if (std::cin.fail() || value <= 0) {
            std::cin.clear();

            // Игнорирует запрос вплоть до символа \n или EOF - в зависимости от
            // того, что наступит раньше.
            /*
             *  1 - задаёт максимальное количество игнорируемых символов.
             * Фактические сообщается cin, что количество символов, которое
             * следует игнорнировать не ограничено. 2 - \n задаёт разделитель,
             * т.е. символ, после которого cin перестаёт игнорировать.
             */
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Try again!" << '\n';
        } else if (value > 0)
            break;
        else {
            std::cout << "Wrong number!" << '\n';
            std::exit(EXIT_FAILURE);
        }
    }
    return value;
}

int chooseExpression() {
    int expression;

    do {
        std::cout << "Input 0-9: ";
        std::cin >> expression;

        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Try again.";
        }
    } while (expression > 9 || expression < 0);

    return expression;
}
