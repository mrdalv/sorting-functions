#ifndef ADDITIONALFUNCS
#define ADDITIONALFUNCS

#include <iostream>
#include <random>

#include "constants.h"

/*
Объявлять параметры по умолчанию рекомендуется в предварительном объявлении
(заголовочный файл), а не в определении функции.
*/

// Генерирует случайные числа с заданным диапазоном чисел
void generateValues(std::vector<int>& vectorValues, int* arrayValues,
                    std::size_t length);

// Эта функция возвращает будущий размер массива
std::size_t enterLength();

//Эта функция сравнения, даёт возможность выполнить сортировку в порядке
//возрастания
bool ascending(int a, int b);

//Эта функция сравнения, даёт возможность выполнить сортировку в порядке
//убывания
bool descending(int a, int b);

// Проверяет целочисленное значение бьез знака на правильность ввода
int getIntValue();

// Эта функция возвращает целочисленное значение для последующей выборки
// алгоритма сортировки
int chooseExpression();

#endif