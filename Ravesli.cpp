#include "Ravesli.h"

int main(int argc, char** argv) {
    // Test
    // int arr[8] = {9, 1, 2, 4, 5, 1, 7, 0};
    // bubbleSort(arr, 8, ascending);
    // for (size_t i = 0; i < 8; ++i) {
    //     std::cout << arr[i] << ' ';
    // }
    // std::cout << std::endl;

    // int array[] =
    // {24,66,123,2,543,234,345,67,3,221,432,53,1,3212,553,22,865,6}; const int
    // length = sizeof(array)/sizeof(array[0]);

    auto length{enterLength()};

    // указатель values станет нулевым, если динамическое выделение одномерного
    // массива не выполнится
    auto* arrayValues{new (std::nothrow) int[length]{}};
    std::vector<int> vectorValues;
    generateValues(vectorValues, arrayValues, length);

    if (!arrayValues) {
        std::cout << "Could not allocate memory!"
                  << "\n";
        std::exit(EXIT_FAILURE);
    }

    switch (chooseExpression()) {
        case 0:
            selectionSort(arrayValues, length, descending);
            break;

        case 1:
            bubbleSort(arrayValues, length, descending);
            break;

        case 2:
            coctailSort(arrayValues, length, descending);
            break;

        case 3:
            combSort(arrayValues, length, descending);
            break;

        case 4:
            insertSort(arrayValues, length, descending);
            break;

        case 5:
            shellSort(arrayValues, length, descending);
            break;

        case 6:
            gnomeSort(arrayValues, length, descending);
            break;

        case 7:
            mergeSortSTL(vectorValues.begin(), vectorValues.end());
            break;

        case 8:
            quickSort(arrayValues, length, descending);
            break;
        default:
            break;
    }

    // Вывод массива
    // for (std::size_t i{}; i < length; ++i)
    //     std::cout << arrayValues[i] << std::endl;

    // delete[] arrayValues;
    // arrayValues = nullptr;

    // Вывод вектора
    for (auto i : vectorValues) {
        std::cout << i << std::endl;
    }

    return 0;
}